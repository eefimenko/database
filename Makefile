HEADERS = 
LSOURCE = main.cpp condition_parser.cpp  condition_parser_test.cpp token.cpp date.cpp database.cpp node.cpp test_runner.cpp
SOURCE = $(LSOURCE) 
LOBJ  = test_runner.o main.o condition_parser.o  condition_parser_test.o token.o date.o database.o node.o
OBJ = $(LOBJ) 
BIN = main
INC = 
LIB = 
CPPFLAGS = -Wall -O3 -g3 -ggdb3 -msse2 -mfpmath=sse -std=c++11
LDFLAGS =
LIBS = 
CC = g++

all: build

build: $(BIN)

$(BIN): $(OBJ)
	$(CC) -o $(BIN) $(LDFLAGS) $(OBJ) $(LIBS) $(INCLIB)

%.o : %.cpp
	$(CC) -c $(INC) $(CPPFLAGS) $< -o $@

clean:
	@echo "Cleaning"
	rm $(BIN); rm $(OBJ); rm *~

submit: 
	cp *.h *.cpp ./submission
	zip -r submission.zip submission
