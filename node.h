#pragma once
#include <string>
#include <memory>
#include "date.h"

using namespace std;

enum class Comparison
{
    Less, LessOrEqual, Greater, GreaterOrEqual, Equal, NotEqual,
};

enum class LogicalOperation
{
    And, Or,
};

enum class NodeType
{
    Empty, EventComparison, DateComparison, LogicalOperation
};

class Node {
public:
Node(const NodeType& type) : type_(type){};
    virtual int Evaluate(const Date& date, const string& event) const = 0;
    NodeType getType() {return type_;};
private:
    NodeType type_;
};

class EmptyNode : public Node {
public:
EmptyNode():
    Node(NodeType::Empty){};
    int Evaluate(const Date& date, const string& event) const override
    {
	return 1;
    };
};

class EventComparisonNode : public Node
{
public:
EventComparisonNode(const Comparison& cmp, const string& value):
    Node(NodeType::EventComparison),
	event_(value),
	cmp_(cmp){};
    int Evaluate(const Date& date, const string& event) const override;
private:
    const string event_;
    const Comparison cmp_;
};

class DateComparisonNode : public Node
{
public:
DateComparisonNode(const Comparison& cmp, const string& value) : Node(NodeType::DateComparison), date_(value), cmp_(cmp) {};
DateComparisonNode(const Comparison& cmp, const Date& date) :Node(NodeType::DateComparison), date_(date), cmp_(cmp){};
    int Evaluate(const Date& date, const string& event) const override;
    
private:
    const Date date_;
    const Comparison cmp_;
};

class LogicalOperationNode : public Node
{
public:
LogicalOperationNode(const LogicalOperation& op, shared_ptr<Node> left, shared_ptr<Node> right) :
    Node(NodeType::LogicalOperation),
	left_(left),
	right_(right),
	op_(op) 	
	{};
    int Evaluate(const Date& date, const string& event) const override;
private:
    shared_ptr<Node> left_, right_;
    LogicalOperation op_;
};
